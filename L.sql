SELECT first.name AS student_1, second.name AS student_2, first.city
FROM student first
JOIN student second ON first.name > second.name
AND first.city = second.city
