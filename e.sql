SELECT name, amount_of_students
FROM course
WHERE is_online IS TRUE
AND amount_of_students BETWEEN 27 AND 310
ORDER BY name DESC, amount_of_students DESC