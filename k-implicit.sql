SELECT student.name AS student_name,
       course.name AS course_name,
       college.name AS student_college,
       student_on_course.student_rating
FROM student_on_course, student, course, college
WHERE student_on_course.student_id = student.id 
    AND student_on_course.course_id = course.id
    AND student.college_id = college.id
    AND student_on_course.student_rating > 50
    AND college.size > 5000
ORDER BY student_name, course_name