SELECT name, amount_of_students
FROM course
ORDER BY 
    CASE WHEN amount_of_students = 300 
         THEN amount_of_students
    END,
    amount_of_students         
LIMIT 3 